#!/usr/bin/env python3

from distutils.core import setup


setup(
    name="installation-birthday",
    author="Chris Lamb",
    author_email="lamby@debian.org",
    scripts=("installation-birthday",),
)
